const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;




module.exports = merge(common,  {



    plugins: [
        new webpack.DefinePlugin({
            //we can use this environment varibale in our code to differentiate behaviour in different environments (i.e. connecting to the correct project etc.)
            'process.env.NODE_ENV': JSON.stringify('development'),
            'ENV': JSON.stringify('dev')

        }),
        //new BundleAnalyzerPlugin()
    ],
     

});
