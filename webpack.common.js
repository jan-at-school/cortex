const path = require('path');
const ExtractTextPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: './src/index.tsx',
    output: {
        filename: 'bundle.js',
        publicPath: "",
        path: __dirname + "/build",
    },
    module: {

        
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            
            // this rule handles images
            {
                test: /\.jpe?g$|\.gif$|\.ico$|\.png$|\.svg$/,
                use: "file-loader?name=[name].[ext]"
            },
            // the following 3 rules handle font extraction
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            },
            {
                test: /\.otf(\?.*)?$/,
                use:
                    "file-loader?name=/fonts/[name].  [ext]&mimetype=application/font-otf"
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html",
            filename: "../build/index.html",
            title: "Veeve",
            chunks: ["app"],
            inject: true
        }),
        new CopyWebpackPlugin([
            {
                from: "./src/res",
                to: "../build/res"
            }
        ]),
        new ExtractTextPlugin({
            filename: "[name].[contenthash].css"
        }),
    ]
};
