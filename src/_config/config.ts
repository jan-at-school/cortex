
declare const ENV;


console.log(ENV);


export const Config = {
    env: ENV,
    isMock: ENV === 'mock',
    ...require(`./config.${ENV}.json`)
};