import { createMuiTheme, StyleRules } from '@material-ui/core/styles';
import { Config } from '../../_config/config';

const fontSize = 14 // px
// Tell Material-UI what's the font-size on the html element.
// 16px is the default font-size used by browsers.
const htmlFontSize = 16
const coef = fontSize / 14;
const pxToRem = size => `${(size / htmlFontSize) * coef}rem`

export const DefaultTheme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: '#452873',
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
      contrastText: '#ffffff',
    },
    secondary: {
      light: '#43B02A',
      main: '#43B02A',
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#ffffff',
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,


  },

  typography: {
    fontFamily: 'Raleway, Roboto',
    color: "#404040",
    fontSize: pxToRem(13),
    body1: {
      fontFamily: 'Raleway',
      color: "#6F7271",
      contrastText: '#ffffff',
      fontSize: pxToRem(13)
    },
    body2: {
      fontFamily: 'Roboto',
      color: "#404040",
      contrastText: '#ffffff',
      fontSize: pxToRem(13)
    },
    button: {
      fontFamily: '"Raleway", "Roboto", "Helvetica", "Arial", "sans-serif"',
      fontWeight: "bolder",
      fontSize: "0.875rem",
      lineHeight: 1.75,
      letterSpacing: "0.02857em",
      textTransform: "none",
      contrastText: '#ffffff'
    },
    pxToRem: pxToRem
  },
} as StyleRules);



// reponsive fonts
DefaultTheme.typography.h1 = {
  fontSize: pxToRem(54),
  fontWeight: "bolder",
  color: "#404040",
  contrastText: '#ffffff',
  [DefaultTheme.breakpoints.down('md')]: {
    fontSize: pxToRem(34),
    fontWeight: "bold",
    color: "#404040",
    contrastText: '#ffffff',
  },
};

DefaultTheme.typography.h2 = {
  fontSize: pxToRem(44),
  fontWeight: "bolder",
  color: "#404040",
  contrastText: '#ffffff',
  [DefaultTheme.breakpoints.down('md')]: {
    fontSize: pxToRem(30),
    color: "#404040",
    contrastText: '#ffffff',
  },
};

DefaultTheme.typography.h3 = {
  fontSize: pxToRem(24),
  color: "#343E47",
  fontWeight: "bolder",
  contrastText: '#ffffff',
};


DefaultTheme.typography.h4 = {
  fontSize: pxToRem(17),
  color: "#343E47",
  fontWeight: "bolder",
  contrastText: '#ffffff',
};

DefaultTheme.typography.h5 = {
  fontSize: pxToRem(17),
  color: "#343E47",
  fontWeight: "bolder",
  contrastText: '#ffffff',
};




if (Config.env != 'prod') {
  (window as any).theme = DefaultTheme
}