
export class OurColors {
    public static primary = '#282f3b'


    public static tertiary = '#BAE8E8'
    public static secondary = '#4a176e'
    public static button = {
        highlight: '#FFD803'
    }
    public static paragraph = {
        default: '#2D334A'
    }
    public static background = "#F7F7F7"
    public static selected = "#5A5F6D"
    public static headlineStrokeButtonText = '#272343'


    public static success = '#43B02A'
    public static warning    = 'yellow'
    public static danger = 'red'

}