import * as React from "react"
import * as ReactDOM from "react-dom"
import { App } from "./App"
import "./index.css"
import { Provider } from 'react-redux';
import { store } from "./redux/store/store";
import { Config } from './_config/config';


ReactDOM.render(
  <div>
    <Provider store={store}>
      <App />
    </Provider>
  </div >,
  document.getElementById("root")
);


