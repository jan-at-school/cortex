
import { createStore, combineReducers } from "redux";
import { authReducer, IAuthState } from '../reducers/auth-reducer';
import { IUIState, uiReducer } from '../reducers/ui-reducer';
import { IBaseState, baseReducer } from "../reducers/base-reducer";



export interface IAppState {
    base: IBaseState,
    auth: IAuthState,
    ui: IUIState
}
const combined = combineReducers({
    base: baseReducer,
    auth: authReducer,
    ui: uiReducer
})

export const store = createStore(combined, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());
export const getStore = () => {
    return (store as any) as IAppState
}