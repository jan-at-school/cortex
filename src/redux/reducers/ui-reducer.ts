

import { User } from 'firebase';
import { AuthService } from '../../services/auth.service';

export const UIActions = {
    TOGGLE_DRAWER: "TOGGLE_DRAWER",
    SINGED_IN: 'SINGED_IN'
}



export interface IUIState {
    drawerOpen: boolean,
    drawerVisible: boolean
    topbarVisible: boolean
}
const user = AuthService.getTokenFromCache();
let initialState: IUIState = {
    drawerOpen: false,
    drawerVisible: true,
    topbarVisible: true
}
export const uiReducer = (state: IUIState = initialState, action) => {

    let rt: IUIState = null
    switch (action.type) {

        case UIActions.TOGGLE_DRAWER:
            rt = { ...state }
            // update state
            rt.drawerOpen = !rt.drawerOpen
            return rt
        case UIActions.SINGED_IN:
            rt = { ...state }
            // update state
            rt.drawerVisible = true
            rt.topbarVisible = true
            return rt
        default:
            return state;
    }
}

