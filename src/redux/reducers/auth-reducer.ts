import { AuthService } from '../../services/auth.service';




export const AuthActions = {

    AUTH_SIGNEDIN: 'AUTH_SIGNEDIN',

    AUTH_SIGNEDOUT: 'AUTH_SIGNEDOUT'
}


export interface IAuthState {
    token: any
}

let initialState = {
    token: AuthService.getTokenFromCache()
}

export const authReducer = (state: IAuthState = initialState, action) => {

    let rt: IAuthState = null
    switch (action.type) {
        case AuthActions.AUTH_SIGNEDIN:
            rt = { ...state }
            rt.token = action.token
            return rt
        case AuthActions.AUTH_SIGNEDOUT:
            rt = { ...state }
            rt.token = null
            return rt
        default:
            return state;
    }
}

