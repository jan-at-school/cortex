import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from "react-router";
import { User } from 'firebase';

interface Props {
    token: User
}

interface State {

}

const withAuthorization = (Component, userType: 'user' | 'admin' = 'user') => {

    class WithAuthorization extends React.Component<Props, State> {

        constructor(props) {
            super(props);

            this.state = {
            }

        }

        componentDidMount() {
        }

        componentWillReceiveProps(newProps) {

        }

        render() {

            const { ...rest } = this.props;
            return this.props.token ? <Component  {...rest} /> : <Redirect to={"/login"}/>
        }
    }

    const mapStateToProps = (state) => ({
        token: state.auth.token,
    });

    const mapDispatchToProps = (dispatch) => ({
    });

    return connect(mapStateToProps, mapDispatchToProps)(WithAuthorization)
}

export default withAuthorization