import * as React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { DefaultTheme } from './res/themes/default-theme';
import { ThemeProvider } from '@material-ui/core';
import { IAppState } from './redux/store/store';
import { OurColors } from './res/colors';
import withAuthorization from './app/auth.hoc';
import { FirebaseService } from './services/firebase.service';
import { auth, User } from 'firebase';
import { AllConfigsScreens } from './components/screens/all-configs/all-configs.screen';
import { AuthActions } from './redux/reducers/auth-reducer';
import { Sidebar } from './components/shared/drawer.component';
import { withStyles } from '@material-ui/core/styles';
import { appStyles } from './app.styles';
import { Topbar } from './components/shared/topbar/topbar.component';
import clsx from 'clsx';
import { SignInScreen } from './components/screens/signin.screen';
import { NewConfig } from './components/screens/new-config/new-config.screen';
import { JobsScreen } from './components/screens/jobs/jobs.screen';
import { HomeScreen } from './components/screens/home/home.screen';



FirebaseService.initialize() // important... don't initialize again

interface Props {
	classes: any,
	sidebar: any,
	drawerOpen: boolean,
	drawerVisible: boolean,
	topbarVisible: boolean,
	setSignedIn(user: any)
}

interface State { }

class Component extends React.Component<Props, State> {
	constructor(props) {
		super(props)

		this.state = {
		}
	}

	componentDidMount() {

	}

	render() {
		const { classes } = this.props;
		const contentStyle: any = { transition: 'margin-left 450ms cubic-bezier(0.23, 1, 0.32, 1)' };
		if (this.props.sidebar) {
			contentStyle.marginLeft = '400px';
		}
		const { drawerOpen } = this.props
		return (

			<ThemeProvider theme={DefaultTheme}>
				<div style={{ backgroundColor: OurColors.background, minHeight: '100vh', display: 'flex' }} >

					{
						this.props.topbarVisible
						&&
						<Topbar position="fixed"
							className={clsx(classes.appBar, {
								[classes.appBarShift]: drawerOpen,
							})} />
					}

					{
						this.props.drawerVisible
						&&

						<Sidebar variant="permanent"
							anchor={'left'}
							open={drawerOpen}
							className={clsx(classes.drawer, {
								[classes.drawerOpen]: drawerOpen,
								[classes.drawerClose]: !drawerOpen,
							})}
							classes={{
								paper: clsx({
									[classes.drawerOpen]: drawerOpen,
									[classes.drawerClose]: !drawerOpen,
								}),
							}} />
					}
					<main className={clsx(classes.content, {
					})} >
						<div className={classes.toolbar} />

						<BrowserRouter>
							<div>
								<Route exact path="/" component={withAuthorization(HomeScreen)} />
								<Route exact path="/login" component={SignInScreen} />
								<Route exact path="/new-config" component={withAuthorization(NewConfig)} />
								<Route exact path="/all-configs" component={withAuthorization(AllConfigsScreens)} />
								<Route exact path="/tasks" component={withAuthorization(JobsScreen)} />
							</div>
						</BrowserRouter>
					</main>

				</div>
				<div>footer</div>
			</ThemeProvider>
		)
	}
}


const ComponentWithStyles = withStyles(appStyles as any)(Component);

const mapStateToProps = (state: IAppState) => {
	return {
		token: state.auth.token,
		drawerOpen: state.ui.drawerOpen,
		drawerVisible: state.ui.drawerVisible,
		topbarVisible: state.ui.topbarVisible
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
	}
}

export const App = connect(
	mapStateToProps,
	mapDispatchToProps)(ComponentWithStyles);