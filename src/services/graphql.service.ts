import { Config } from '../_config/config';





export class GraphQLService {



    public static performGraphQLQuery(token, queryName: string, query: string, variables: any) {
        return fetch(Config.github,
            {
                method: 'POST',
                body: JSON.stringify({
                    "query": query,
                    variables: variables
                }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res).data[queryName]
            })
            .catch(e => {
                console.error(e);
            })
    }

}   