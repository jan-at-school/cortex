import { Config } from "../_config/config";
import firebase = require("firebase");





export class FirebaseService {

    static initialize() {
        // Configure Firebase.
        const config = Config.firebaseConfig;
        firebase.initializeApp(config);

    }
    static getFireStore(){
        return firebase.firestore();
    }
    static getStorage(){
        return firebase.storage();
    }


}
