import * as firebase from 'firebase';
import { User } from 'firebase';
import { GraphQLService } from './graphql.service';
import { store, getStore } from '../redux/store/store';
import { AuthService } from './auth.service';

import { v4 as uuidv4 } from 'uuid';


export class MainService {


    public static base = 'http://dev.efectura.com:9999'

    public static itemTypes = MainService.base + '/api/Item/GetItemsTypes'
    public static itemTypeFamily = MainService.base + '/api/Family/GetFamily'
    public static attributes = MainService.base + '/api/Attribute/GetAttribute'
    public static associations = MainService.base + '/api/AssociationType/GetAssociationType'
    public static save_config = 'http://161.35.203.138:8080/save-config'
    public static csvColumns = 'http://161.35.203.138:8080/get-csv-columns'
    public static files = 'http://161.35.203.138:8080/files'
    public static configs = 'http://161.35.203.138:8080/configs'
    public static submit = 'http://161.35.203.138:8080/submit'
    public static fetch = 'http://161.35.203.138:8080/fetch'
    
    static async getItemTypes() {
        return fetch(MainService.itemTypes,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${AuthService.getTokenFromCache()}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res).Value
            })
            .catch(e => {
                console.error(e);
            })
    }


    static async getItemTypeFamilyID(itemTypeNumber) {
        return fetch(`${MainService.itemTypeFamily}?itemTypeNumber=${itemTypeNumber}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${AuthService.getTokenFromCache()}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res => {
                if (res.StatusCode == 200 && res.Value[0].Id) {
                    return res.Value[0].Id
                }
                else {
                    throw new Error('no 200')
                }
            })
            .catch(e => {
                console.error(e);
            })
    }


    static async getAttributes(familyId) {
        return fetch(`${MainService.attributes}?familyId=${familyId}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${AuthService.getTokenFromCache()}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res).Value
            })
            .catch(e => {
                console.error(e);
            })
    }


    static async getAssociations(itemTypeNumber) {
        return fetch(`${MainService.associations}?itemTypeNumber=${itemTypeNumber}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${AuthService.getTokenFromCache()}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res).Value
            })
            .catch(e => {
                console.error(e);
            })
    }






    static async retrieveFiles() {
        return fetch(`${MainService.files}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${AuthService.getTokenFromCache()}`
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res) //files
            })
            .catch(e => {
                console.error(e);
            })
    }

    static async getCSVColums(filePath, separator) {
        return fetch(`${MainService.csvColumns}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    filePath: filePath,
                    separator: separator
                })
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)[0].split(';') //columns
            })
            .catch(e => {
                console.error(e);
            })
    }



    static async saveConfig(config) {
        config.configId = uuidv4()
        return fetch(`${MainService.save_config}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(config)
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res=>{
                return true
            })
            .catch(e => {
                console.error(e);
            })
    }



    static async getAllConfigs() {
        return fetch(`${MainService.configs}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res=>{
                return res
            })
            .catch(e => {
                console.error(e);
            })
    }

    static async sumbitTask(config) {
        return fetch(`${MainService.submit}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(config)
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res=>{
                return res
            })
            .catch(e => {
                console.error(e);
            })
    }


    static async fetchTask() {
        return fetch(`${MainService.fetch}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res=>{
                return res
            })
            .catch(e => {
                console.error(e);
            })
    }


}