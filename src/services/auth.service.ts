import * as firebase from 'firebase';
import { User } from 'firebase';
import { GraphQLService } from './graphql.service';




export class AuthService {


    public static authURL = 'http://dev.efectura.com:9999/api/Account/Login'

    static async signOut() {
        localStorage.clear()
        return true
    }


    static async signIn(username: string, password: string, shouldSave: boolean = false) {
        return fetch(AuthService.authURL,
            {
                method: 'POST',
                body: JSON.stringify({
                    userName: username,
                    password: password
                }),
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(res => res.text())
            .then(res => {
                console.log(JSON.parse(res));
                return JSON.parse(res)
            })
            .then(res=>{
                if(res.StatusCode == 200){
                    return res.Value.access_token
                }
                else return null;
            })
            .catch(e => {
                console.error(e);
            })
            .then(AuthService.cacheToken(true))
    }


    static cacheToken(save: boolean) {
        return async (token) => {
            if (save) {
                localStorage.setItem('token',token)
            }
            return token;
        }
    }
    static getTokenFromCache() {
        try {
            return localStorage.getItem('token') ? localStorage.getItem('token') : false
        }
        catch (e) {
            return false
        }
    }
}