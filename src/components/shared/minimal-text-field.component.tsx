import { InputBase } from "@material-ui/core"
import React = require("react")

export const MinimalTextField = (props) => {
    return (
        <InputBase
            {...props} style={{ background: '#eaeaea',borderRadius:'5px' ,padding:'2px 4px',fontSize:'inherit',margin:'5px',}} />
    )
}