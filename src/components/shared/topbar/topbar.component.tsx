import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { OurIcons } from "../../../res/icons";
import { IAppState } from "../../../redux/store/store";
import Typography from "@material-ui/core/Typography";
import MenuIcon from '@material-ui/icons/Menu';
import Button from "@material-ui/core/Button";
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import { UIActions } from '../../../redux/reducers/ui-reducer';

const styles = theme => ({
    root: {
        width: '100%',
        background: 'white'
    },
});

interface Props {
    classes: any,
    history: any,
    token: string
    toggleDrawer()
}

interface State {
}
class Component extends React.Component<Props, State> {

    constructor(props) {
        super(props)
        this.state = {

        }

    }

    componentDidMount() {
    }
    render() {
        const { classes, token } = this.props
        return (

            <AppBar {...this.props}>
                <Toolbar
                    className={classes.toolbar}>

                    <Grid container style={{ height: "100%", margin: 'auto' }} justify="space-between" alignItems="center" xs={12}>

                        <Grid item>
                            <IconButton aria-label="menu" onClick={() => this.props.toggleDrawer()}>
                                <MenuIcon />
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <Button
                                startIcon={<AccountCircleOutlinedIcon />}>
                                Profile
                                </Button>
                        </Grid>


                    </Grid>

                </Toolbar>

            </AppBar>

        )
    }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleDrawer: () => dispatch({ type: UIActions.TOGGLE_DRAWER })
    }
}
export const Topbar = connect(
    mapStateToProps,
    mapDispatchToProps
)(ComponentWithStyles);