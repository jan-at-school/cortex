import * as React from "react"
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import AddIcon from '@material-ui/icons/Add';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';
import { Drawer, ListItemIcon, Link } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { IAppState } from "../../redux/store/store";
import { OurColors } from '../../res/colors';
import { OurImages } from '../../res/images';
import { Config } from '../../_config/config';


const styles = theme => ({
    root: {
        width: '100%',
        height: '100vh',
        color: 'white',
        background: OurColors.primary,
    },
});

interface Props {
    classes: any,
    history: any,
    drawer: any,
}

interface State {
}
class Component extends React.Component<Props, State> {

    constructor(props) {
        super(props)
        this.state = {

        }

    }

    componentDidMount() {
    }
    render() {
        const { classes, drawer } = this.props
        return (

            <Drawer
                {...this.props}>
                <div className={classes.root}>
                    <List>
                        <ListItem button key={'dashboard'} style={{ textAlign: 'center' }}>
                            {/* <ListItemIcon><img src={OurImages.logo} style={{ width: 30 }} /></ListItemIcon> */}

                            <ListItemText ><img src={OurImages.logo} style={{ width: 30, display: 'inline', verticalAlign: 'middle' }} />&nbsp;&nbsp;&nbsp;<Typography style={{ display: 'inline', color: 'white', letterSpacing: 5, verticalAlign: 'middle' }}>CSV Module</Typography></ListItemText>
                        </ListItem>
                        <div style={{ height: '30px' }}></div>
                        <Link href="/new-config">
                            <ListItem button key={'dashboard'} >
                                <ListItemIcon style={{ color: 'white' }}><AddIcon /></ListItemIcon>
                                <ListItemText style={{ color: 'white' }}><Typography color="inherit">New Config</Typography></ListItemText>
                            </ListItem>
                        </Link>

                        <Link href="/all-configs">
                            <ListItem button key={'dashboard'}>
                                <ListItemIcon style={{ color: 'white' }}><PlaylistPlayIcon /></ListItemIcon>
                                <ListItemText style={{ color: 'white' }}><Typography color="inherit">All Configs</Typography></ListItemText>
                            </ListItem>
                        </Link>

                        <Link href="/tasks">
                            <ListItem button key={'dashboard'}>
                                <ListItemIcon style={{ color: 'white' }}><AssignmentTurnedInIcon /></ListItemIcon>
                                <ListItemText style={{ color: 'white' }}><Typography color="inherit">Tasks</Typography></ListItemText>
                            </ListItem>
                        </Link>
                    </List>
                </div>
            </Drawer>
        )
    }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
    return {
        drawerOpen: state.ui
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}
export const Sidebar = connect(
    mapStateToProps,
    mapDispatchToProps
)(ComponentWithStyles);