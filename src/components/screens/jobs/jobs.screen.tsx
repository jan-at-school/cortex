import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import { Topbar } from "../../shared/topbar/topbar.component";

import { IAppState } from "../../../redux/store/store";
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";
import { OurColors } from '../../../res/colors';
import { Container, Card, CircularProgress } from '@material-ui/core';
import { MainService } from '../../../services/main.service';

const styles = theme => ({
  root: {
    width: '100%',
  },
  topMargin: {
    marginTop: '30px'
  },

});

interface Props {
  classes: any,
  history: any,
  user: User
}

interface State {
  tasks: any[],
  loading: boolean
}
class Component extends React.Component<Props, State> {

  constructor(props) {
    super(props)
    this.state = {
      tasks: [],
      loading: true
    }

  }


  componentDidMount() {

    MainService.fetchTask()
      .then(tasks => {
        this.setState({
          tasks: tasks,
          loading: false
        })
      })
  }

  render() {

    const { classes, user } = this.props

    return (

      <span>
        <Container>

          <div style={{ height: '50px' }}>

          </div>

          <Typography style={{ fontSize: '1.8rem', fontWeight: 'bolder', textAlign: 'center' }}>Tasks</Typography>

          {
            this.state.tasks.length
            &&
            <Grid container spacing={2}>

              {
                this.state.tasks.map(task => {
                  return (
                    <Grid item xs={3}>
                      <Card style={{ padding: 10, textAlign: 'center' }}>
                        <Typography style={{ fontSize: '1.5rem', fontWeight: 'bolder', textAlign: 'center' }}>{task.status}</Typography>
                        <Typography style={{ textAlign: 'center' }}>{task.taskId}</Typography>
                      </Card>
                    </Grid>)
                })
              }
            </Grid>
          }


          {

            this.state.loading
            &&
            <div style={{ textAlign: 'center' }}>
              <CircularProgress></CircularProgress>
            </div>
          }
        </Container>
      </span>

    )
  }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
  return {
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}


export const JobsScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(ComponentWithStyles);
