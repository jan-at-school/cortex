import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import { IAppState } from "../../redux/store/store";
import Grid from "@material-ui/core/Grid";
import { MinimalTextField } from '../shared/minimal-text-field.component';
import { InputLabel, Link, Button, CircularProgress } from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { AuthService } from '../../services/auth.service';
import { AuthActions } from '../../redux/reducers/auth-reducer';
import { UIActions } from '../../redux/reducers/ui-reducer';

const styles = theme => ({
    root: {
        width: '100%',
    },
    textField: {
        borderRadius: '5px',
        padding: '5px 5px',
        backgroundColor: 'white',
        fontSize: 'inherit',
        width: '100%',
        margin: 'auto',
        ...theme.typography.body2,
    },
    title: {
        textAlign: 'center',
        letterSpacing: 5,
        ...theme.typography.h2
    }
});

interface Props {
    classes: any,
    history: any,
    user: User
    dispatchUserSignedIn(user)
}

interface State {
    passwordVisible: boolean,
    username: string,
    password: string,
    remember: boolean,
    loading: boolean,
    error: string
}
class Component extends React.Component<Props, State> {

    constructor(props) {
        super(props)
        this.state = {
            passwordVisible: false,
            username: '',
            password: '',
            remember: true,
            loading: false,
            error: null
        }

    }


    componentDidMount() {

        console.log('sigin');
        
    }

    handleSignIn = () => {
        this.setState({ loading: true, error: null })
        AuthService.signIn(this.state.username, this.state.password,this.state.remember)
            .then(this.onSignedIn)
            .catch(e => {
                this.setState({ loading: false, error: 'Please try again' })
            })
    }

    onSignedIn = async (user) => {
        if (user) {
            this.props.dispatchUserSignedIn(user)
            this.props.history.push('/')
        }
        return user
    }


    handleUsernameChange = (e) => {
        this.setState({ username: e.target.value })
    }
    handlePasswordChange = (e) => {
        this.setState({ password: e.target.value })
    }
    render() {

        const { classes, user } = this.props

        return (

            <Grid container justify="center" alignItems="center" style={{ height: '80vh' }}>
                <Grid item style={{ height: '100%' }} />
                <Grid item style={{ width: '100%', maxWidth: '350px', margin: 'auto' }}>
                    <Paper style={{ width: '100%', padding: '20px', backgroundColor: '#eaeaea' }}>


                        <Typography className={classes.title}>CSV Module</Typography>
                        <div>
                            <Typography>Username</Typography>
                            <InputBase className={classes.textField} onChange={this.handleUsernameChange} />
                        </div>

                        <div>

                            <Typography>Password</Typography>
                            <InputBase type={this.state.passwordVisible ? 'text' : 'password'} className={classes.textField} onChange={this.handlePasswordChange} />
                        </div>

                        <Grid container justify="space-between" alignItems="center">
                            <Grid item>
                                <Typography>
                                    <Checkbox
                                        checked={this.state.remember}
                                        color="primary"
                                        size="small"
                                        onChange={(e) => this.setState({ remember: e.target.checked })}
                                    /> Remember Me
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography>
                                    <Link
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => this.setState({ passwordVisible: !this.state.passwordVisible })}>
                                        {this.state.passwordVisible ? 'Hide Password' : 'Show Password'}
                                    </Link>
                                </Typography>
                            </Grid>
                        </Grid>

                        <Button fullWidth color="primary" variant="contained" onClick={this.handleSignIn} disabled={this.state.loading}>

                            {
                                this.state.loading
                                    ?
                                    <CircularProgress size="20px"/>
                                    :
                                    'Sign In'
                            }



                        </Button>
                    </Paper>
                </Grid>
                <Grid item style={{ height: '100%' }} />

            </Grid>

        )
    }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatchUserSignedIn: (token) => {
            dispatch({ type: AuthActions.AUTH_SIGNEDIN, token: token })
            dispatch({ type: UIActions.SINGED_IN })
        }
    }
}
export const SignInScreen = connect(
    mapStateToProps,
    mapDispatchToProps
)(ComponentWithStyles);
