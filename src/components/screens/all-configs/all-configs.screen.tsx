import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import { Topbar } from "../../shared/topbar/topbar.component";

import { IAppState } from "../../../redux/store/store";
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";
import { OurColors } from '../../../res/colors';
import { MainService } from '../../../services/main.service';
import { Container, Card, CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    width: '100%',
  },
  topMargin: {
    marginTop: '30px'
  },

});

interface Props {
  classes: any,
  history: any,
  user: User
}

interface State {
  configs: any[],
  loading: boolean
}
class Component extends React.Component<Props, State> {

  constructor(props) {
    super(props)
    this.state = {
      configs: [],
      loading: true
    }

  }


  componentDidMount() {

    MainService.getAllConfigs()
      .then(configs => {
        this.setState({ configs: configs, loading: false })
      })

  }

  handleRun = async (config) => {
    MainService.sumbitTask(config)
      .then(done => {
        this.props.history.push('/tasks')
      })
  }

  render() {

    const { classes, user } = this.props

    return (

      <span>
        <Container>

          <div style={{ height: '50px' }}></div>

          
          <Typography style={{ fontSize: '1.8rem', fontWeight: 'bolder', textAlign: 'center' }}>Configs</Typography>
          {
            this.state.configs.length
            &&

            <Grid container spacing={2}>

              {
                this.state.configs.map(config => {
                  return (
                    <Grid item xs={3}>
                      <Card style={{ padding: 10, textAlign: 'center' }}>
                        <Typography style={{ fontSize: '1.5rem', fontWeight: 'bolder' }}>{config.configName}</Typography>
                        <Button onClick={()=>this.handleRun(config)} color="primary">Run</Button>
                      </Card>
                    </Grid>)
                })
              }
            </Grid>
          }


          {

            this.state.loading
            &&
            <div style={{ textAlign: 'center' }}>
              <CircularProgress></CircularProgress>
            </div>
          }
        </Container>
      </span>

    )
  }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
  return {
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}


export const AllConfigsScreens = connect(
  mapStateToProps,
  mapDispatchToProps
)(ComponentWithStyles);
