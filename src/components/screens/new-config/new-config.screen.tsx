import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import { Topbar } from "../../shared/topbar/topbar.component";

import { IAppState } from "../../../redux/store/store";
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";
import { OurColors } from '../../../res/colors';
import { Container, Card, Button, InputLabel, CircularProgress } from '@material-ui/core';
import { MainService } from '../../../services/main.service';
import { AuthService } from '../../../services/auth.service';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        width: '100%',
    },

});

interface Props {
    classes: any,
    history: any,
    user: User
}

interface State {
    itemTypes: any,
    csvFile: any,
    columnNames: string[],
    attributes: any[],
    associations: any[],
    files: string[],
    error: string,
    loading: boolean,

    finalConfiguration: {
        configId: string,
        configMode:string,
        csvFilePath: string,
        csvSeparator: string,
        configName: string,
        fletumConfiguration: {
            associationCode: string,
            associationCol: string,
            attributeColumnMap: { [key: string]: string },
            category: string,
            familyId: 0,
            itemType: 0,
            skuCol: string
        }
    }



}
class Component extends React.Component<Props, State> {

    constructor(props) {
        super(props)
        this.state = {
            itemTypes: null,
            columnNames: null,
            attributes: null,
            associations: null,
            files: null,
            csvFile: null,
            error: null,

            loading: false,

            finalConfiguration: {
                configId: Math.random() + '',
                configName: '',
                configMode:'csv',
                csvFilePath: null,
                csvSeparator: null,
                fletumConfiguration: {
                    associationCode: null,
                    associationCol: null,
                    attributeColumnMap: {},
                    category: 'fletum',
                    familyId: null,
                    itemType: null,
                    skuCol: null
                }
            }
        }

    }

    componentDidMount() {
        this.setState({ loading: true })
        AuthService.signIn('admin', 'admin')
            .then(this.retriveAllItemTypes)


    }

    retriveAllItemTypes = async () => {

        this.setState({ loading: true })
        return MainService.getItemTypes()
            .then(itemTypes => {
                this.setState({ itemTypes: itemTypes, loading: false })
                console.log('item types');

            })
    }


    handleItemTypeChoosen = async (itemType) => {
        this.state.finalConfiguration.fletumConfiguration.itemType = itemType
        this.setState({ loading: true })
        MainService.getItemTypeFamilyID(itemType)
            .then(familyId => {
                console.log('familyId', familyId);
                this.state.finalConfiguration.fletumConfiguration.familyId = familyId
                this.setState({})
            })
            .then(this.retrieveFiles)
            .catch(err => {
                this.setState({ loading: false })
                alert("Fletum did not repond successfully. Please choose another item type.")
                this.setState({ error: "Please choose another item type." })
            })

    }


    retrieveFiles = async () => {
        const files = await MainService.retrieveFiles()
        this.setState({ files: files, loading: false })
    }

    handleFileChoosen = (file) => {
        this.state.finalConfiguration.csvFilePath = file
        this.setState({})
    }

    handleSeparatorChoosen = async (Separator) => {
        this.state.finalConfiguration.csvSeparator = Separator;
        this.setState({ loading: true })
        MainService.getCSVColums(this.state.finalConfiguration.csvFilePath, Separator)
            .then(columns => {
                this.setState({ columnNames: columns })
            })
            .then(this.retrieveAttributes)
            .then(this.retrieveAsssociations)
            .then(done => {
                this.setState({ loading: false })
            })

    }


    retrieveAttributes = async () => {
        MainService.getAttributes(this.state.finalConfiguration.fletumConfiguration.familyId)
            .then(attributes => {
                console.log('attributes', attributes);

                this.setState({ attributes: attributes })
            })

    }


    retrieveAsssociations = async () => {
        MainService.getAssociations(this.state.finalConfiguration.fletumConfiguration.itemType)
            .then(associations => {
                this.setState({ associations: associations })
            })
    }


    handleSubmit = () => {
        MainService.saveConfig(this.state.finalConfiguration)
            .then(done => {
                this.props.history.push('/all-configs')
            })

    }

    render() {

        const { classes, user } = this.props

        return (

            <span>
                <div style={{ height: '100px' }}> </div>

                <Container>
                    {
                        this.state.itemTypes
                        &&
                        <Grid container>
                            <Grid item >

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Select Item Type</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>
                                    {
                                        Object.keys(this.state.itemTypes).map(itemType =>
                                            (<Button
                                                color="primary"
                                                variant={(this.state.itemTypes[itemType] == this.state.finalConfiguration.fletumConfiguration.itemType) ? 'contained' : 'text'}
                                                onClick={() => this.handleItemTypeChoosen(this.state.itemTypes[itemType])}>
                                                {itemType}
                                                {/* {this.state.itemTypes[itemType]} */}
                                            </Button>))
                                    }
                                </div>
                            </Grid>
                        </Grid>

                    }





                    {
                        this.state.files
                        &&
                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Select File</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>
                                    {
                                        this.state.files.map(file => {

                                            const pathSplit = file.split('/');
                                            return (<Button
                                                color="primary"
                                                variant={(file == this.state.finalConfiguration.csvFilePath) ? 'contained' : 'text'}
                                                onClick={() => this.handleFileChoosen(file)}>
                                                {pathSplit[pathSplit.length - 1]}
                                            </Button>)
                                        }
                                        )
                                    }
                                </div>
                            </Grid>
                        </Grid>
                    }




                    {

                        this.state.finalConfiguration.csvFilePath
                        &&

                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Select Separator</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>

                                    <Button
                                        color="primary"
                                        variant={(',' == this.state.finalConfiguration.csvSeparator) ? 'contained' : 'text'}
                                        onClick={() => this.handleSeparatorChoosen(',')}>
                                        Comma ','
                                        </Button>

                                    <Button
                                        color="primary"
                                        variant={('\t' == this.state.finalConfiguration.csvSeparator) ? 'contained' : 'text'}
                                        onClick={() => this.handleSeparatorChoosen('\t')}>
                                        Tab
                                        </Button>
                                </div>
                            </Grid>
                        </Grid>

                    }


                    {
                        this.state.finalConfiguration.fletumConfiguration.familyId
                        &&
                        this.state.columnNames
                        &&
                        this.state.attributes
                        &&

                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Choose SKU Column</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>

                                    <FormControl
                                        variant="outlined">
                                        <InputLabel style={{ backgroundColor: OurColors.background }}>Column Name</InputLabel>
                                        <Select
                                            value={this.state.finalConfiguration.fletumConfiguration.skuCol}
                                            style={{ minWidth: 300 }}
                                            onChange={(e) => {
                                                this.state.finalConfiguration.fletumConfiguration.skuCol = e.target.value as string
                                                this.setState({})
                                            }}
                                        >
                                            {
                                                this.state.columnNames.map(columnName => {
                                                    return (<MenuItem value={columnName}>{columnName}</MenuItem>)
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                            </Grid>
                        </Grid>
                    }




                    {

                        this.state.finalConfiguration.fletumConfiguration.skuCol
                        &&

                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Choose Attributes Columns</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>

                                    <Grid container>

                                        {
                                            this.state.attributes.map(attribute => {
                                                const attributeColumnMap = this.state.finalConfiguration.fletumConfiguration.attributeColumnMap
                                                return (
                                                    <Grid container justify="space-between" alignItems="center">
                                                        <Grid item xs={6}>
                                                            <Typography>{attribute.Code}</Typography>
                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            <FormControl style={{ width: '100%', marginTop: 15 }} variant="outlined" >
                                                                <InputLabel style={{ backgroundColor: OurColors.background }}>Column Name</InputLabel>
                                                                <Select
                                                                    value={attributeColumnMap[attribute.Code]}
                                                                    onChange={(e) => {
                                                                        attributeColumnMap[attribute.Code] = e.target.value as string
                                                                        this.state.finalConfiguration.fletumConfiguration.attributeColumnMap = attributeColumnMap
                                                                        this.setState({})
                                                                    }}
                                                                >
                                                                    {
                                                                        this.state.columnNames.map(columnName => {
                                                                            return (<MenuItem value={columnName}>{columnName}</MenuItem>)
                                                                        })
                                                                    }
                                                                </Select>
                                                            </FormControl>
                                                        </Grid>
                                                    </Grid>
                                                )
                                            })
                                        }

                                    </Grid>

                                </div>
                            </Grid>
                        </Grid>

                    }



                    {


                        this.state.finalConfiguration.fletumConfiguration.skuCol
                        &&
                        !Object.values(this.state.finalConfiguration.fletumConfiguration.attributeColumnMap).filter(att => att ? false : true).length
                        &&

                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Choose Associations</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>
                                    <Grid container justify="space-between" alignItems="center" spacing={2}>
                                        <Grid item xs={6}>
                                            <FormControl
                                                style={{ width: '100%' }}
                                                variant="outlined">
                                                <InputLabel style={{ backgroundColor: OurColors.background }}>Association Code</InputLabel>
                                                <Select
                                                    variant="outlined"
                                                    style={{ minWidth: 300 }}
                                                    value={this.state.finalConfiguration.fletumConfiguration.associationCode}
                                                    onChange={(e) => {
                                                        this.state.finalConfiguration.fletumConfiguration.associationCode = e.target.value as string
                                                        this.setState({})
                                                    }}
                                                >
                                                    {
                                                        this.state.associations.map(association => {
                                                            return (<MenuItem value={association.Code}>{association.Code}</MenuItem>)
                                                        })
                                                    }
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <FormControl variant="outlined" style={{ width: '100%' }}>
                                                <InputLabel style={{ backgroundColor: OurColors.background }}>Association Column Name</InputLabel>
                                                <Select
                                                    style={{ minWidth: 300 }}
                                                    value={this.state.finalConfiguration.fletumConfiguration.associationCol}
                                                    onChange={(e) => {
                                                        this.state.finalConfiguration.fletumConfiguration.associationCol = e.target.value as string
                                                        this.setState({})
                                                    }}
                                                >
                                                    {
                                                        this.state.columnNames.map(columnName => {
                                                            return (<MenuItem value={columnName}>{columnName}</MenuItem>)
                                                        })
                                                    }
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>

                                </div>
                            </Grid>
                        </Grid>

                    }

                    {

                        this.state.finalConfiguration.fletumConfiguration.associationCode &&
                        this.state.finalConfiguration.fletumConfiguration.associationCol
                        &&

                        <Grid container>
                            <Grid item xs={12}>

                                <Typography style={{ fontSize: '1.5rem', textAlign: 'center', marginTop: 20 }}>Name Config</Typography>
                                <div style={{ width: '100%', padding: 20, textAlign: 'center' }}>
                                    <TextField value={this.state.finalConfiguration.configName}
                                        style={{ minWidth: 300 }}
                                        label="Config Name"
                                        onChange={(e) => {
                                            this.state.finalConfiguration.configName = e.target.value
                                            this.setState({})
                                        }}></TextField>

                                    <Button onClick={this.handleSubmit}>
                                        Submit
                                        </Button>

                                </div>
                            </Grid>
                        </Grid>

                    }

                    {

                        this.state.loading
                        &&
                        <div style={{ textAlign: 'center' }}>
                            <CircularProgress></CircularProgress>
                        </div>
                    }




                </Container>


            </span>

        )
    }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}


export const NewConfig = connect(
    mapStateToProps,
    mapDispatchToProps
)(ComponentWithStyles);
