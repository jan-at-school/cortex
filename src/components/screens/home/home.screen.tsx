import * as React from "react"
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';


import { User } from 'firebase';
import { withStyles } from "@material-ui/core/styles";
import { Topbar } from "../../shared/topbar/topbar.component";

import { IAppState } from "../../../redux/store/store";
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";
import { OurColors } from '../../../res/colors';

const styles = theme => ({
  root: {
    width: '100%',
  },
  topMargin: {
    marginTop: '30px'
  },
  
});

interface Props {
  classes: any,
  history: any,
  user: User
}

interface State {
}
class Component extends React.Component<Props, State> {

  constructor(props) {
    super(props)
    this.state = {

    }

  }


  componentDidMount() {


  }

  render() {

    const { classes, user } = this.props

    return (

      <span>

      </span>

    )
  }
}


const ComponentWithStyles = withStyles(styles as any)(Component);
const mapStateToProps = (state: IAppState) => {
  return {
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}


export const HomeScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(ComponentWithStyles);
