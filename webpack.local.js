const merge = require('webpack-merge');
const dev = require('./webpack.dev.js');
const webpack = require('webpack');
path = require('path');


module.exports = merge(dev, {


    mode: "development",
    // Enable sourcemaps for debugging webpack's output.

    devtool: 'inline-source-map',
    optimization: {
        minimize: false
    },

    plugins: [
        new webpack.DefinePlugin({
            //we can use this environment varibale in our code to differentiate behaviour in different environments (i.e. connecting to the correct project etc.)
            'process.env.NODE_ENV': JSON.stringify('development'),
        })],

    devServer: {
        contentBase: path.resolve(__dirname, ""),
        historyApiFallback: true,
        watchContentBase: true

    }

});