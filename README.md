# CSV Module Frontend


## Test Locally

Install ngrok and connect port 3000

then

`npm run build`
`npm start`

## Update

`npm run build`




## Project Stucture



#### res

Gets copied to build folder and can be accessed through url `/res/images/some-image.png`


* Use **OurImages**  class to import images
* Use **OurStrings**  class for strings (Will be helpful in translations etc)
* Use **OurIcons**  class for icons
* Use **OurColors**  class for colors
* Place pwa and favicons etc in `src/res/pwa` and edit the index.html




```


<-------- MASTER CONFIG PROCESS START -----> 


{
  "configId": "string",  --> UUID generated UI SIDE
  "configMode": "string", --> MARK AS "csv"
  "configName": "string", --> STEP 9 
  "csvFilePath": "string", --> STEP 4
  "csvSeparator": "", --> STEP 4 
 
  "fletumConfiguration": { 
    "associationCode": "string", --> STEP 8 --> CODE FROM RESPONSE
    "associationCol": "string", --> STEP 8 ---> COLUMN NAME FROM COLUMNS 
  
    "attributeColumnMap": {            ------> STEP 7 
      "ColumnCode": "Variable_code",
    },
    "category": "string",
    "familyId": 0, --> STEP 3 --> FAMILY --> ID 
    "itemType": 0, --> STEP 3 --> ItemType
    "skuCol": "string" --> STEP 6 --> SKU COL 
  }
}



1. Login for Token (POST /api/Account/Login)
-- Login with admin admin
-- Use Bearer Token as AUTH

2. Get All ItemTypes (GET /api/Item/GetItemsTypes) ---> ASK USER FOR AN ITEM TYPE FROM THE LIST OF TYPES.
-- You will show all item types to user
-- User will select one item type based on the name

{
  "Value": {
    "Product": 0,
    "Location": 1,
    "MRP": 2,
    "PushNotification": 3,
    "File": 4
  },
  "StatusCode": 200
}


3. Get Families based on ItemType (/api/Family/GetFamily) --> THEN GO TO ANOTHER SCREEN AND SHOW FAMILIES FOR THAT ITEM TYPE
-- If Not 200 and Has no "Value" then say "No family in fletum or Fletum has problems fetching family for this itemtype" 
-- If it is 200, You will get the ID and mark the it as family Id 


4. Open a File Explorer Local PC
--> USER WILL SELECT CSV FILE  
--> /get-csv-columns for that CSV FILE PATH and it will send array of string values which are columns.
{
  "filePath": "string",
  "separator": ","
}

5. You will call GET ATTRIBUTES using the familyId /api/Attribute/GetAttribute?familyId=4
--> You will get list of attributes.
--> You will get the list "Code" Element from the Attributes and make a list

6. YOU WILL ASK FOR SKU COLUMN
--> USER WILL SELECT A COL From LIST OF COLUMNS. 

7. YOU WILL NOW SHOW A SCREEN TO USER IN WHICH ONE SIDE HAS ATTRIBUTE CODES AND ON THE OTHER SIDE WE HAVE COLUMN NAMES
--> User will match AttributeCode and ColumnName
--> You will create the Map from it.

8. YOU WILL NOW GET A LIST OF ASSOCITIONS TYPES PER ITEM_TYPE and Column Name of Associated Column
--> /AssociationType/GetAssociationType?itemTypeNumber=1 
--> You will get a list of CODES from the response AND SHOW IT TO User
--> USER WILL SELECT A CODE FROM THE LIST
--> USER WILL ALSO SELECT A COLUMN FROM THE COLUMN NAMES 

9. ASK USER FOR CONFIG NAME AND GENERATE UI SIDE UUID to CONFIG-ID

<-------- MASTER CONFIG PROCESS END -----> 


10. SEND CONFIG TO http://localhost:8080/save-config JAVA API.

11. CONFIG WILL SAVED BY API, THEN U CAN CALL GET-COFIGS and it will give u the config


12.  NOW YOU WILL SHOW THE USER A SCREEN FOR CONFIGS which get configs from backend (GET-COFIGS ENDPOINT )


13. USER CAN SELECT A CONFIG FROM THOSE CONFIGS AND RUN IT AS A TASK 


14. API CALL TO JAVA API FOR http://localhost:8080/submit
{
 // CONFIGURATION WE ALREADY HAVE FROM CONFIG ENDPOINT
 
  "configuration": {
    "configId": "string",
    "configMode": "string",
    "configName": "string",
    "csvFilePath": "string",
    "csvSeparator": "",
    "fletumConfiguration": {
      "associationCode": "string",
      "associationCol": "string",
      "attributeColumnMap": {
        "additionalProp1": "string",
        "additionalProp2": "string",
        "additionalProp3": "string"
      },
      "category": "string",
      "familyId": 0,
      "itemType": 0,
      "skuCol": "string"
    }
  },
  "errorMessage": "",
}

15. SO THERE WILL BE ANOTHER TAB WHICH WILL HAVE THE RUNNING TASKS.


```


## Deployment

`cp -r ./build /srv/csv_module_ui`

`cp ./nginx.conf /etc/nginx/conf.d/csv_module_ui.conf`

